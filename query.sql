Create table Users(
	Uuid UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	Id int IDENTITY(1,1),
    LastName varchar(255),
    FirstName varchar(255),
    Password varchar(50),
    Email varchar (255)
    Type varchar(50),
    active int
)

Create table Groups(
	Uuid UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	Id int IDENTITY(1,1),
    Number varchar(25),
    active int
)

Create table Subjects(
	Uuid UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	Id int IDENTITY(1,1),
    Code varchar(25),
    Name varchar(25),
    active int
)

Create table GroupSubject(
	Uuid UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	Id int IDENTITY(1,1),
    GroupsId varchar(255),
    SubjectId varchar(255),
    TeacherId varchar(255),
    active int
)

Create table GroupStudent(
	Uuid UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	Id int IDENTITY(1,1),
    GroupSubjectId varchar(255),
    StudentId varchar(255),
    active int
)

Create table Qualifications(
	Uuid UNIQUEIDENTIFIER PRIMARY KEY default NEWID(),
	Id int IDENTITY(1,1),
    GroupStudentId varchar(255),
    Score varchar(255),
    active int
)