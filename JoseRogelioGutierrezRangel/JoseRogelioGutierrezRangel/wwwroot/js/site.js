﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.


function goView(view) {
    if (view == 'user') {
        window.location.href = '/Users';
    } else if (view == 'group') {
        window.location.href = '/Groups';
    } else if (view == 'subject') {
        window.location.href = '/Subjects';
    } else if (view == 'assign') {
        window.location.href = '/Assignment';
    } else if (view == 'qualification') {
        window.location.href = '/Qualification';
    } else if (view == 'signoff') {
        localStorage.clear();
        window.location.href = '/Home';
    }
}

function getSession() {
    if (localStorage.getItem('username') == null) {
        window.location.href = '/Home';
    }

    getMenu();
}

function getMenu() {
    if (localStorage.getItem('username') != null) {
        if (JSON.parse(localStorage.getItem("username"))['type'] == 'administrador') {
            $("#viewUser").css("display", "block");
            $("#viewGroup").css("display", "block");
            $("#viewSubject").css("display", "block");
            $("#viewAssign").css("display", "block");
            $("#viewQualification").css("display", "none");
        } else {
            $("#viewUser").css("display", "none");
            $("#viewGroup").css("display", "none");
            $("#viewSubject").css("display", "none");
            $("#viewAssign").css("display", "none");
            $("#viewQualification").css("display", "block");
        }
    }
}

function getPermit() {
    if (localStorage.getItem('username') != null) {
        if (JSON.parse(localStorage.getItem("username"))['type'] == 'administrador') {
            $("#viewUser").css("display", "block");
        } else {
            $("#viewUser").css("display", "none");
        }
    }
}