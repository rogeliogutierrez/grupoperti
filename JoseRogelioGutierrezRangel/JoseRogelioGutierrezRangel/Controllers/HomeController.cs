﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using JoseRogelioGutierrezRangel.Models;

namespace JoseRogelioGutierrezRangel.Controllers
{
    public class HomeController : Controller
    {

        private DBCtx Context { get; }
        public HomeController(DBCtx _context)
        {
            this.Context = _context;
        }

        public IActionResult Index()
        {
            return View();
        }

    }
}
