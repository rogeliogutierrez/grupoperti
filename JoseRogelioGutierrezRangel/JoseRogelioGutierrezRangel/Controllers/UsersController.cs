﻿using Microsoft.AspNetCore.Mvc;
using JoseRogelioGutierrezRangel.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JoseRogelioGutierrezRangel.Controllers
{
    public class UsersController : Controller
    {
        private DBCtx Context { get; }
        public UsersController(DBCtx _context)
        {
            this.Context = _context;
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}
