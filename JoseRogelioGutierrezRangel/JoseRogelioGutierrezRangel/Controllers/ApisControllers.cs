﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using JoseRogelioGutierrezRangel.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace JoseRogelioGutierrezRangel.Controllers
{
    [Produces("application/json")]
    [Route("api/university")]
    public class ApisControllers : Controller
    {
        private readonly DBCtx context;

        public ApisControllers(DBCtx context)
        {
            this.context = context;
        }


        [HttpGet("list/user")]
        public IEnumerable<User> GetUser()
        {
            return context.Users.Where(x => x.Type != "administrador").ToList();
        }

        [HttpGet("listteacher")]
        public IEnumerable<User> GetTeacher()
        {
            return context.Users.Where(x => x.Type == "maestro").Where(x=>x.Active == 1).ToList();
        }

        [HttpGet("liststudent")]
        public IEnumerable<User> GetStudent()
        {
            return context.Users.Where(x => x.Type == "alumno").Where(x => x.Active == 1).ToList();
        }

        [HttpGet("teacher/student/{uuid}")]
        public IEnumerable<Assing> GetTeacherStudent(string uuid)
        {
            IList<Assing> assingList = new List<Assing>();
            foreach(var groupSubject in context.GroupSubject.Where(x=>x.TeacherId == uuid))
            {
                foreach(var groupStudent in context.GroupStudent.Where(x => x.GroupSubjectId == groupSubject.Uuid.ToString()))
                {
                    var group = context.Groups.Where(x => x.Uuid == new Guid(groupSubject.GroupsId)).First();
                    var subject = context.Subjects.Where(x => x.Uuid == new Guid(groupSubject.SubjectId)).First();
                    var user = context.Users.Where(x => x.Uuid == new Guid(groupStudent.StudentId)).First();
                    Assing assing = new Assing();

                    assing.Uuid = groupStudent.Uuid;
                    assing.Number = (group != null) ? group.Number : "";

                    assing.Code = (subject != null) ? subject.Code : "";
                    assing.Name = (subject != null) ? subject.Name : "";

                    assing.FirstName = (user != null) ? user.FirstName : "";
                    assing.LastName = (user != null) ? user.LastName : "";
                    assing.Email = (user != null) ? user.Email : "";
                    assingList.Add(assing);
                }

            }

            return assingList;
        }

        [HttpGet("student/{uuid}")]
        public IEnumerable<Assing> GetIdStudent(string uuid)
        {
            IList<Assing> assingList = new List<Assing>();
            
            foreach (var groupStudent in context.GroupStudent.Where(x => x.StudentId == uuid.ToString()))
            {
                var groupSubject = context.GroupSubject.Where(x => x.Uuid == new Guid(groupStudent.GroupSubjectId)).First();

                var group = context.Groups.Where(x => x.Uuid == new Guid(groupSubject.GroupsId)).First();
                var subject = context.Subjects.Where(x => x.Uuid == new Guid(groupSubject.SubjectId)).First();
                var user = context.Users.Where(x => x.Uuid == new Guid(groupStudent.StudentId)).First();
                Assing assing = new Assing();

                assing.Uuid = groupStudent.Uuid;
                assing.Number = (group != null) ? group.Number : "";

                assing.Code = (subject != null) ? subject.Code : "";
                assing.Name = (subject != null) ? subject.Name : "";

                assing.FirstName = (user != null) ? user.FirstName : "";
                assing.LastName = (user != null) ? user.LastName : "";
                assing.Email = (user != null) ? user.Email : "";
                assingList.Add(assing);
            }

            return assingList;
        }

        [HttpPost("disableduser")]
        public IActionResult DisabledUser(string id)
        {
            var user = context.Users.FirstOrDefault(x => x.Uuid == new Guid(id));
            user.Active = user.Active == 1 ? 0 : 1; 

            context.Entry(user).State = EntityState.Modified;
            context.SaveChanges();
            return Json(new { status = "200", responseText = "The user modify" });
        }

        [HttpPost("adduser")]
        public IActionResult PostUser(User user)
        {
            if (ModelState.IsValid)
            {
                var exist = context.Users.Where(x => x.Email == user.Email).ToList();
                if (exist.Count() <= 0)
                {
                    user.Uuid = Guid.NewGuid();
                    context.Users.Add(user);
                    context.SaveChanges();
                    return Json(new { status = "200", responseText = "The user create" });
                }
                else
                {
                    return Json(new { status = "205", responseText = "The user exist" });
                }
            }

            return Json(new { status = "205", responseText = "The create user error" });
        }

        /* Group */
        [HttpGet("listgroup")]
        public IEnumerable<Group> GetGroup()
        {
            return context.Groups.ToList();
        }

        [HttpPost("disabledgroup")]
        public IActionResult DisabledGroup(string id)
        {
            var group = context.Groups.FirstOrDefault(x => x.Uuid == new Guid(id));
            group.Active = group.Active == 1 ? 0 : 1;

            context.Entry(group).State = EntityState.Modified;
            context.SaveChanges();
            return Json(new { status = "200", responseText = "The group modify" });
        }

        [HttpPost("addgroup")]
        public IActionResult PostGroup(Group group)
        {
            if (ModelState.IsValid)
            {
                var exist = context.Groups.Where(x => x.Number == group.Number).ToList();
                if (exist.Count() <= 0)
                {
                    group.Uuid = Guid.NewGuid();
                    context.Groups.Add(group);
                    context.SaveChanges();
                    return Json(new { status = "200", responseText = "The group create" });
                }
                else
                {
                    return Json(new { status = "205", responseText = "The group exist" });
                }
            }

            return Json(new { status = "205", responseText = "The create group error" });
        }

        /* Subject */
        [HttpGet("listsubject")]
        public IEnumerable<Subject> GetSubject()
        {
            return context.Subjects.ToList();
        }

        [HttpPost("disabledsubject")]
        public IActionResult DisabledSubject(string id)
        {
            var subject = context.Subjects.FirstOrDefault(x => x.Uuid == new Guid(id));
            subject.Active = subject.Active == 1 ? 0 : 1;

            context.Entry(subject).State = EntityState.Modified;
            context.SaveChanges();
            return Json(new { status = "200", responseText = "The subject modify" });
        }

        [HttpPost("addsubject")]
        public IActionResult PostSubject(Subject subject)
        {
            if (ModelState.IsValid)
            {
                var exist = context.Subjects.Where(x => x.Code == subject.Code).Where(x => x.Name == subject.Name).ToList();
                if (exist.Count() <= 0)
                {
                    subject.Uuid = Guid.NewGuid();
                    context.Subjects.Add(subject);
                    context.SaveChanges();
                    return Json(new { status = "200", responseText = "The group create" });
                }
                else
                {
                    return Json(new { status = "205", responseText = "The group exist" });
                }
            }

            return Json(new { status = "205", responseText = "The create group error" });
        }

        /* Assing */
        [HttpGet("listassing")]
        public IEnumerable<Assing> GetAssing()
        {
            IList<Assing> assingList = new List<Assing>();
            foreach (var groupSubject in context.GroupSubject)
            {
                Assing assing = new Assing();

                var group = context.Groups.Where(x => x.Uuid == new Guid(groupSubject.GroupsId)).First();
                var subject = context.Subjects.Where(x => x.Uuid == new Guid(groupSubject.SubjectId)).First();
                var user = context.Users.Where(x => x.Uuid == new Guid(groupSubject.TeacherId)).First();

                assing.Uuid = groupSubject.Uuid;
                assing.Number = (group != null) ? group.Number : "";

                assing.Code = (subject != null) ? subject.Code : "";
                assing.Name = (subject != null) ? subject.Name : "";

                assing.FirstName = (user != null) ? user.FirstName : "";
                assing.LastName = (user != null) ? user.LastName : "";
                assing.Email = (user != null) ? user.Email : "";

                assing.Active = groupSubject.Active;
                assingList.Add(assing);
            }

            return assingList.ToList();
        }

        [HttpGet("listassingstudent/{uuid}")]
        public IEnumerable<AssingStudent> GetAssingStudent(string uuid)
        {
            IList<AssingStudent> assingList = new List<AssingStudent>();
            foreach (var groupStudent in context.GroupStudent.Where(x=>x.GroupSubjectId == uuid))
            {
                AssingStudent assingStudent = new AssingStudent();

                var user = context.Users.Where(x => x.Uuid == new Guid(groupStudent.StudentId)).First();

                assingStudent.Uuid = groupStudent.Uuid;

                assingStudent.FirstName = (user != null) ? user.FirstName : "";
                assingStudent.LastName = (user != null) ? user.LastName : "";
                assingStudent.Email = (user != null) ? user.Email : "";

                assingStudent.Active = groupStudent.Active;
                assingList.Add(assingStudent);
            }

            return assingList.ToList();
        }

        [HttpPost("disabledassing")]
        public IActionResult DisabledAssing(string id)
        {
            var groupSubject = context.GroupSubject.FirstOrDefault(x => x.Uuid == new Guid(id));
            groupSubject.Active = groupSubject.Active == 1 ? 0 : 1;

            context.Entry(groupSubject).State = EntityState.Modified;
            context.SaveChanges();
            return Json(new { status = "200", responseText = "The subject modify" });
        }

        [HttpPost("addassing")]
        public IActionResult PostAssing(GroupSubject groupSubject)
        {
            if (ModelState.IsValid)
            {
                var exist = context.GroupSubject.Where(x => x.GroupsId == groupSubject.GroupsId)
                    .Where(x => x.SubjectId == groupSubject.SubjectId)
                    .Where(x => x.TeacherId == groupSubject.TeacherId)
                    .ToList();
                if (exist.Count() <= 0)
                {
                    groupSubject.Uuid = Guid.NewGuid();
                    context.GroupSubject.Add(groupSubject);
                    context.SaveChanges();
                    return Json(new { status = "200", responseText = "The assigin create" });
                }
                else
                {
                    return Json(new { status = "205", responseText = "The assigin exist" });
                }
            }

            return Json(new { status = "205", responseText = "The create assigin error" });
        }

        [HttpPost("addsubjectstudent")]
        public IActionResult PostSubjectStudent(GroupStudent groupStudent)
        {
            if (ModelState.IsValid)
            {
                var exist = context.GroupStudent.Where(x => x.GroupSubjectId == groupStudent.GroupSubjectId)
                    .Where(x => x.StudentId == groupStudent.StudentId)
                    .ToList();
                if (exist.Count() <= 0)
                {
                    groupStudent.Uuid = Guid.NewGuid();
                    context.GroupStudent.Add(groupStudent);
                    context.SaveChanges();
                    return Json(new { status = "200", responseText = "The subject, student create" });
                }
                else
                {
                    return Json(new { status = "205", responseText = "The subject, student exist" });
                }
            }

            return Json(new { status = "205", responseText = "The create subject, student error" });
        }

        /* Qualification */

        [HttpGet("list/qualification/{uuid}")]
        public IEnumerable<Qualification> GetQualification(string uuid)
        {
            return context.Qualifications.Where(x => x.GroupStudentId == uuid).ToList();
        }

        [HttpPost("add/qualification")]
        public IActionResult PostQualification(Qualification qualification)
        {
            if (ModelState.IsValid)
            {
                qualification.Uuid = Guid.NewGuid();
                context.Qualifications.Add(qualification);
                context.SaveChanges();
                return Json(new { status = "200", responseText = "The user create" });
            }

            return Json(new { status = "205", responseText = "The create user error" });
        }

        /* Login */
        [HttpPost("login")]
        public IActionResult Valid(User user)
        {
            /*var output = new List<User>
            {
                new User { Email = user.Email, Password = user.Password },
            };*/
            var output = context.Users.Where(x => x.Email == user.Email).Where(x => x.Password == user.Password).ToList();
            if(output.Count() > 0)
            {
                if (output.First().Active == 1)
                {
                    return Json(new { status = "200", responseText = "The user exist", data = output.First() });
                }
                else
                {
                    return Json(new { status = "204", responseText = "The user desactive" });
                }
            }
            return Json(new { status = "205", responseText = "The user not exist" });
            
        }
    }
}
