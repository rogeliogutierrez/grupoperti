﻿using System;
namespace JoseRogelioGutierrezRangel.Models
{
    public class Subject
    {
        public Int32 ID { get; set; }
        public Guid Uuid { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public Int32 Active { get; set; }
    }
}
