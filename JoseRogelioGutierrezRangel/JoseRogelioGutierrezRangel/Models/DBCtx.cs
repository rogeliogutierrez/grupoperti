﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;

namespace JoseRogelioGutierrezRangel.Models
{
    public class DBCtx : DbContext
    {
        private readonly DbContextOptions _options;

        public DBCtx(DbContextOptions options) : base(options)
        {
            _options = options;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

        }

        
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<GroupSubject> GroupSubject { get; set; }
        public DbSet<GroupStudent> GroupStudent { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
    }
}
