﻿using System;
namespace JoseRogelioGutierrezRangel.Models
{
    public class GroupStudent
    {
        public Int32 ID { get; set; }
        public Guid Uuid { get; set; }
        public string GroupSubjectId { get; set; }
        public string StudentId { get; set; }
        public Int32 Active { get; set; }
    }
}
