﻿using System;
namespace JoseRogelioGutierrezRangel.Models
{
    public class GroupSubject
    {
        public Int32 ID { get; set; }
        public Guid Uuid { get; set; }
        public string GroupsId { get; set; }
        public string SubjectId { get; set; }
        public string TeacherId { get; set; }
        public Int32 Active { get; set; }
    }
}
