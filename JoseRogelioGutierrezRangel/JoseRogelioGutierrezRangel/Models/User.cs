﻿using System;
namespace JoseRogelioGutierrezRangel.Models
{
    public class User
    {
        public Int32 ID { get; set; }
        public Guid Uuid { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
        public Int32 Active { get; set; }
    }
}