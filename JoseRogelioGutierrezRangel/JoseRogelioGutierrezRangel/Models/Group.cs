﻿using System;
namespace JoseRogelioGutierrezRangel.Models
{
    public class Group
    {
        public Int32 ID { get; set; }
        public Guid Uuid { get; set; }
        public string Number { get; set; }
        public Int32 Active { get; set; }
    }
}
