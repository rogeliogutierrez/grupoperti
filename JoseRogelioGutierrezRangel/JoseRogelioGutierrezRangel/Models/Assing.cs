﻿using System;
namespace JoseRogelioGutierrezRangel.Models
{
    public class Assing
    {
        public Int32 ID { get; set; }
        public Guid Uuid { get; set; }
        public string Number { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Int32 Active { get; set; }
    }
}
