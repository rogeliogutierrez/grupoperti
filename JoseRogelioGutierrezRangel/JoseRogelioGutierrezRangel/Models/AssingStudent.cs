﻿using System;
namespace JoseRogelioGutierrezRangel.Models
{
    public class AssingStudent
    {
        public Int32 ID { get; set; }
        public Guid Uuid { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Int32 Active { get; set; }
    }
}
