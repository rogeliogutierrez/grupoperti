﻿using System;
namespace JoseRogelioGutierrezRangel.Models
{
    public class Qualification
    {
        public Int32 ID { get; set; }
        public Guid Uuid { get; set; }
        public string GroupStudentId { get; set; }
        public string Score { get; set; }
        public Int32 Active { get; set; }
    }
}
