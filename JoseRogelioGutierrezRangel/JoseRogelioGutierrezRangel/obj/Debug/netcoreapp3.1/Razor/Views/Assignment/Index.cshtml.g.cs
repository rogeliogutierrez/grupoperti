#pragma checksum "/Users/rogeliogutierrez/Documents/Coffeholik/grupoperti/JoseRogelioGutierrezRangel/JoseRogelioGutierrezRangel/Views/Assignment/Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "67b09032c3adb673633efd2d273eed3eb6318e96"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Assignment_Index), @"mvc.1.0.razor-page", @"/Views/Assignment/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "/Users/rogeliogutierrez/Documents/Coffeholik/grupoperti/JoseRogelioGutierrezRangel/JoseRogelioGutierrezRangel/Views/_ViewImports.cshtml"
using JoseRogelioGutierrezRangel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/rogeliogutierrez/Documents/Coffeholik/grupoperti/JoseRogelioGutierrezRangel/JoseRogelioGutierrezRangel/Views/_ViewImports.cshtml"
using JoseRogelioGutierrezRangel.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"67b09032c3adb673633efd2d273eed3eb6318e96", @"/Views/Assignment/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"425d817052cdfe8d384f5b0e3001160b7c5f3e28", @"/Views/_ViewImports.cshtml")]
    public class Views_Assignment_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "/Users/rogeliogutierrez/Documents/Coffeholik/grupoperti/JoseRogelioGutierrezRangel/JoseRogelioGutierrezRangel/Views/Assignment/Index.cshtml"
  
    ViewBag.Title = "Asignacion Grupos, materias, maestros y alumnos";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""container"">
    <h3>Asignación</h3>
    <ul class=""nav nav-tabs"">
        <li id=""viewUser""><a href=""#"" onclick=""goView('user')"">Usuarios</a></li>
        <li id=""viewGroup""><a href=""#"" onclick=""goView('group')"">Grupos</a></li>
        <li id=""viewSubject""><a href=""#"" onclick=""goView('subject')"">Materias</a></li>
        <li class=""active"" id=""viewAssign""><a href=""#"" onclick=""goView('assign')"">Asignación</a></li>
        <li id=""viewQualification""><a href=""#"" onclick=""goView('qualification')"">Calificaciones</a></li>
        <li><a href=""#"" onclick=""goView('signoff')"">Cerrar Sesión</a></li>
    </ul>
    <br>

    <button type=""button"" class=""btn btn-primary"" data-toggle=""modal"" data-target=""#assignModal"">
        Realiza una nueva asignación
    </button>
    <br>
    <table class=""table table-striped"" id=""tableassing"">
        <thead>
            <tr>
                <th>UUID</th>
                <th>Grupo</th>
                <th>Materia</th>
                <th>Maestro</th>
                <th");
            WriteLiteral(@">&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            </tr>
        </tbody>
    </table>
</div>


<div class=""modal fade"" id=""assignModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalLabel"" aria-hidden=""true"">
    <div class=""modal-dialog"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""exampleModalLabel"">Agregar nuevo</h5>
                <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">
                <div class=""form-group"">
                    <label for=""groupsId"">Grupo</label>
                    <select type=""text"" class=""form-control"" id=""groupsId"" name=""groupsId""></select>
                </div>
                <div class=""form-group"">
                    <label f");
            WriteLiteral(@"or=""subjectId"">Materia</label>
                    <select type=""text"" class=""form-control"" id=""subjectId"" name=""subjectId""></select>
                </div>
                <div class=""form-group"">
                    <label for=""teacherId"">Maestro</label>
                    <select type=""text"" class=""form-control"" id=""teacherId"" name=""teacherId""></select>
                </div>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Cerrar</button>
                <button type=""button"" class=""btn btn-primary"" id=""save"">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class=""modal fade"" id=""addStudentModal"" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalLabel"" aria-hidden=""true"">
    <div class=""modal-dialog"" role=""document"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <h5 class=""modal-title"" id=""exampleModalLabel"">Agregar nuevo</h5>
       ");
            WriteLiteral(@"         <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                    <span aria-hidden=""true"">&times;</span>
                </button>
            </div>
            <div class=""modal-body"">

                <input type=""hidden"" class=""form-control"" id=""uuid"" name=""uuid"" placeholder=""uuid boucher"">

                <div class=""form-group"">
                    <label for=""studentId"">Estudiantes</label>
                    <select type=""text"" class=""form-control"" id=""studentId"" name=""studentId""></select>
                </div>

                <table class=""table table-striped"" id=""tableassingstudent"">
                    <thead>
                        <tr>
                            <th>UUID</th>
                            <th>Alumno</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        </tr>
                    </tbody>
                <");
            WriteLiteral(@"/table>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-secondary"" data-dismiss=""modal"">Cerrar</button>
                <button type=""button"" class=""btn btn-primary"" id=""saveStudent"">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        getSession();

        $.ajax({
            type: ""GET"",
            url: 'api/university/listassing',
            async: true,
            success: function (result) {
                console.log(result);
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        var text_btn = result[i].active == ""1"" ? ""Desactivar"" : ""Activar"";
                        var color_btn = result[i].active == '1' ? 'danger' : 'success'
                        $('#tableassing').find('tbody')
                            .append($('<tr><td>' + result[i].uuid + '</td>'+
                                '<");
            WriteLiteral(@"td> ' + result[i].number + '</td>' +
                                '<td> ' + result[i].code + '-' + result[i].name + '</td>' +
                                '<td> ' + result[i].firstName + ' ' + result[i].lastName + '</td>' +
                                '<td><button class=""btn btn-' + color_btn + '"" onclick=""status(\'' + result[i].uuid + '\', \'' + result[i].active + '\')"">' + text_btn + '</button></td>' +
                                '<td><button class=""btn btn-info"" onclick=""addStudent(\'' + result[i].uuid + '\')"">Agregar estudiantes</button></td>'+
                                '</tr>'));
                    }
                }
            }
        });

        $.ajax({
            type: ""GET"",
            url: 'api/university/listgroup',
            async: true,
            success: function (result) {
                console.log(result);
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        $('#groupsId').append($('<op");
            WriteLiteral(@"tion value=""' + result[i].uuid + '"">' + result[i].number + '</option>'));
                    }
                }
            }
        });

        $.ajax({
            type: ""GET"",
            url: 'api/university/listsubject',
            success: function (result) {
                console.log(result)
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        $('#subjectId').append($('<option value=""' + result[i].uuid + '"">' + result[i].code + '-' +result[i].name+  '</option>'));
                    }
                }
            }
        });

        $.ajax({
            type: ""GET"",
            url: 'api/university/listteacher',
            async: true,
            success: function (result) {
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        $('#teacherId').append($('<option value=""' + result[i].uuid + '"">' + result[i].firstName + ' ' + result[i].lastName + ");
            WriteLiteral(@"'</option>'));
                    }
                }
            }
        });

        $.ajax({
            type: ""GET"",
            url: 'api/university/liststudent',
            async: true,
            success: function (result) {
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        $('#studentId').append($('<option value=""' + result[i].uuid + '"">' + result[i].firstName + ' ' + result[i].lastName + '</option>'));
                    }
                }
            }
        });

        $(""#save"").click(function () {
            var flag = true;
            if ($(""#groupsId"").val() == '') {
                flag = false;
                alert(""Favor seleccione un grupo"");
            }

            if ($(""#subjectId"").val() == '') {
                flag = false;
                alert(""Favor seleccione una materia"");
            }

            if ($(""#teacherId"").val() == '') {
                flag = false;
                alert(""Fa");
            WriteLiteral(@"vor seleccione un maestro"");
            }

            if (flag){
                $.ajax({
                    type: ""POST"",
                    url: 'api/university/addassing',
                    data: {
                        groupSubject: {
                            GroupsId: $(""#groupsId"").val(),
                            SubjectId: $(""#subjectId"").val(),
                            TeacherId: $(""#teacherId"").val(),
                            Active: 1
                        }
                    },
                    success: function (result) {
                        console.log(result);
                        if (result.status == ""200"") {
                            window.location.reload();
                        } else if (result.status == ""205"") {
                            alert(""La asignacion realizada ya existe"");
                        }
                    }
                });
            }
        });

        $(""#saveStudent"").click(function () {
            var flag = true;
 ");
            WriteLiteral(@"           if ($(""#studentId"").val() == '') {
                flag = false;
                alert(""Favor seleccione un grupo"");
            }

            if (flag) {
                $.ajax({
                    type: ""POST"",
                    url: 'api/university/addsubjectstudent',
                    data: {
                        groupStudent: {
                            GroupSubjectId: $(""#uuid"").val(),
                            StudentId: $(""#studentId"").val(),
                            Active: 1
                        }
                    },
                    success: function (result) {
                        console.log(result);
                        if (result.status == ""200"") {
                            window.location.reload();
                        } else if (result.status == ""205"") {
                            alert(""La asignacion del alumno a una materia realizada ya existe"");
                        }
                    }
                });
            }
        });
    ");
            WriteLiteral(@"});

    function status(id, active) {
        $.ajax({
            type: ""POST"",
            url: 'api/university/disabledassing',
            data: { id: id },
            success: function (result) {
                console.log(result);
                window.location.reload();
            }
        });
    }

    function addStudent(uuid) {
        $(""#uuid"").val(uuid);
        $('#tableassingstudent tbody').empty();
        $.ajax({
            type: ""GET"",
            url: 'api/university/listassingstudent/' + uuid,
            async: true,
            success: function (result) {
                console.log(result);
                if (result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        var text_btn = result[i].active == ""1"" ? ""Desactivar"" : ""Activar"";
                        var color_btn = result[i].active == '1' ? 'danger' : 'success'
                        $('#tableassingstudent').find('tbody')
                            .append($('<tr><td>' ");
            WriteLiteral(@"+ result[i].uuid + '</td>' +
                                '<td> ' + result[i].firstName + ' ' + result[i].lastName + '</td>' +
                                '<td><button class=""btn btn-' + color_btn + '"" onclick=""status(\'' + result[i].uuid + '\', \'' + result[i].active + '\')"">' + text_btn + '</button></td>' +
                                '</tr>'));
                    }
                }
            }
        });

        $(""#addStudentModal"").modal('show');
    }
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Views_Assignment_Index> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Views_Assignment_Index> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<Views_Assignment_Index>)PageContext?.ViewData;
        public Views_Assignment_Index Model => ViewData.Model;
    }
}
#pragma warning restore 1591
